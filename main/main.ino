#include <PS3BT.h>
#include <Servo.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>

Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *mainmotor = AFMS.getMotor(1);

#define Motor1ON  2
#define Motor2ON  3
#define Motor1F  5 
#define Motor1R  4
#define Motor2F  7
#define Motor2R  6
#define CON_LED  9
#define R_LED 10
#define G_LED 12

Servo Hit;
USB Usb;
BTD Btd(&Usb);
PS3BT PS3(&Btd);

int VinPin = A5;

int Vin = 0;
int valL;                                                                  
int valR; 
int valH;
int set_speed;
int speed_mode;

void init_pwm()
{
        pinMode(Motor1ON, OUTPUT);
		pinMode(Motor2ON, OUTPUT);
		pinMode(Motor1F, OUTPUT);
        pinMode(Motor1R, OUTPUT);
        pinMode(Motor2F, OUTPUT);
        pinMode(Motor2R, OUTPUT);
        pinMode(CON_LED, OUTPUT);
        pinMode(R_LED, OUTPUT);
        pinMode(G_LED, OUTPUT);
}

void setup() {
  if (Usb.Init() == -1) {
    while(1);
  }
    Hit.attach(8);
    init_pwm();
    analogWrite(Motor1ON, 0);
    analogWrite(Motor1F,0);
    analogWrite(Motor1R,0);  
    analogWrite(Motor2ON, 0);
    analogWrite(Motor2F,0);
    analogWrite(Motor2R,0);
    analogWrite(R_LED,0); 
    analogWrite(G_LED,50); 
    speed_mode = 1;
    set_speed = 45;
    Hit.write(90);
    analogWrite(CON_LED, 50);
    AFMS.begin();
        
}
void loop() {
  Usb.Task();
  Vin = analogRead(VinPin);
  if (Vin > 211){
    analogWrite(G_LED, 50);
    analogWrite(R_LED, 0); 
     if(PS3.PS3Connected || PS3.PS3NavigationConnected) {
      analogWrite(CON_LED, 0);
      valR = PS3.getAnalogHat(LeftHatY);
      valL = PS3.getAnalogHat(RightHatY);
      valH = PS3.getAnalogButton(R2);
      valH = map(valH, 0, 255, 90, 40);
      Hit.write(valH);
      
        
       if(PS3.getButtonClick(L1)){
       	Serial.print("Running Motor at 100");
  			mainmotor->setSpeed(100);
  			mainmotor->run(FORWARD);
        }}
        else {
        	Serial.print("Error");
        }

      if (valL >= 120 && valL <= 134){
        analogWrite(Motor2ON, 0);
        analogWrite(Motor2F,0);
        analogWrite(Motor2R,0);      
      }
      if (valL <= 119){
        analogWrite(Motor2ON, 255);
        valL = map(valL, 0, 119, set_speed, 0);
        analogWrite(Motor2F,valL);
        analogWrite(Motor2R,0); 
        valL = 127;      
      }
      if (valL >= 135){
        analogWrite(Motor2ON, 255);
        valL = map(valL, 135, 255, 0, set_speed);
        analogWrite(Motor2R,valL); 
        analogWrite(Motor2F,0);
        valL = 127;      
      }
    }
    
        if(PS3.getButtonClick(PS)) {
      PS3.disconnect();
      analogWrite(Motor1ON, 0);
      analogWrite(Motor1F,0);
      analogWrite(Motor1R,0);  
      analogWrite(Motor2ON, 0);
      analogWrite(Motor2F,0);
      analogWrite(Motor2R,0); 
      speed_mode = 1;
      set_speed = 45;
      Hit.write(90);
      analogWrite(CON_LED, 50);
    } }
    
     
